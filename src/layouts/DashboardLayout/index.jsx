import { Outlet } from "react-router-dom";
import Header from "../Header";
import Sidebar from "../Sidebar";
import "./index.scss";

const DashboardLayout = () => {
  return (
    <div className="main-layout">
      <Header />
      <main className="main-container">
        <div className="sidebar">
          <Sidebar />
        </div>
        <main className="main-outlet">
          <Outlet />
        </main>
      </main>
    </div>
  );
};
export default DashboardLayout;
