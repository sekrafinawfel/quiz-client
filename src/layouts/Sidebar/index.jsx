import { useState } from "react";
import { NavLink } from "react-router-dom";
import sideBarToggleIcon from "../../assets/icons/sidebar-toggle.svg";
import listIcon from "../../assets/icons/list-check.svg"
import usersIcon from "../../assets/icons/user.svg"
import logoutIcon from "../../assets/icons/logout.svg"

import "./index.scss";
const Sidebar = () => {
  const [expend, setExpend] = useState(false);
  return (
    <div
      className={expend ? "sidebar-container expended" : "sidebar-container"}
    >
      <img
        src={sideBarToggleIcon}
        className={
          expend ? "sidebar-toggle-btn rotated-btn" : "sidebar-toggle-btn"
        }
        onClick={() => setExpend(!expend)}
      />
      {/* <NavLink to="/">Dashboard</NavLink> */}
      <div className="pages">
        <NavLink
          to="/quiz-list"
          className={({ isActive }) =>
            isActive ? "link activeStyle" : "link"
          }
        >
          <img src={listIcon} alt="list icon" />

          <span>List</span>
        </NavLink>
        <NavLink
          to="/profile"
          className={({ isActive }) =>
            isActive ? "link activeStyle" : "link"
          }
        >
         <img src={usersIcon} alt="users icon" />
          <span>Profile</span>
        </NavLink>
      </div>
      <button className="sidebar-logout">
       <img src={logoutIcon} alt="logout icon" />
      </button>
    </div>
  );
};

export default Sidebar;
