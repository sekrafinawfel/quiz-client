import "./index.scss";
import TakiQuizzLogo from "../../assets/logos/TakiQuizz125x25.svg";
import UserAvatar from "../../assets/images/nawfel.jpg";
const Header = () => {
  return (
    <div className="navbar-container">
      <img src={TakiQuizzLogo} alt="" />
      <div className="nav-user-info">
        <div>
          <img src={UserAvatar} alt="" />
          <div className="username-role">
            <span className="username">Nawfel Sekrafi</span>
            <span className="role">Admin</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
