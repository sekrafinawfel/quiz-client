import React, { Suspense, Fragment, lazy } from "react";
import { Route, Routes, createBrowserRouter } from "react-router-dom";
import DashboardLayout from "./layouts/DashboardLayout";
import Dashboard from "./pages/Dashboard";
import MyProfile from "./pages/MyProfile";
import QuizList from "./pages/QuizList";
import Quiz from "./pages/Quiz";
import SelectQuizType from "./pages/SelectQuizType";
// import AuthGuard from "./components/AuthGuard";
// import GuestGuard from "./components/GuestGuard";
// import SwitchGuard from "./components/SwitchGuard";

// export const renderRoutes = (routes = []) => (
//   <Suspense fallback={<></>}>
//     <Fragment>
//       {routes.map((route, i) => {
//         const Guard = route.guard || Fragment;
//         const Layout = route.layout || Fragment;
//         const Component = route.component;

//         return (
//           <Routes
//             key={i}
//             path={route.path}
//             exact={route.exact}
//             render={(props) => (
//               <Guard>
//                 <Layout>
//                   {route.routes ? (
//                     renderRoutes(route.routes)
//                   ) : (
//                     <Component {...props} />
//                   )}
//                 </Layout>
//               </Guard>
//             )}
//           />
//         );
//       })}
//     </Fragment>
//   </Suspense>
// );

const routes = createBrowserRouter([
  {
    path: "/",
    element: <DashboardLayout />,
    // errorElement: <ErrorPage />,
    // loader: rootLoader,
    // action: rootAction,
    children: [
      //   {
      //     //   errorElement: <ErrorPage />,
      // children: [
      { index: true, element: <Dashboard /> },

      {
        path: "quiz-list",
        element: <QuizList />,
      },
      {
        path: "quiz-list/:quizId",
        element: <Quiz />,
      },
      {
        path: "/select-quiz-type",
        element: <SelectQuizType />
      },
      {
        path: "/profile",
        element: <MyProfile />,
      },
      //       //     {
      //       //       path: "contacts/:contactId/edit",
      //       //       element: <EditContact />,
      //       //       action: editAction,
      //       //       loader: contactLoader,
      //       //     },
      //       //     {
      //       //       path: "contacts/:contactId/destroy",
      //       //       action: deleteContact,
      //       //       errorElement: <div>Oops! There was an error.</div>,
      //       //     },
      //     ],
      //   },
    ],
  },
]);

export default routes;
