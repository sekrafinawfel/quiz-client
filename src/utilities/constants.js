import quizPrevieImg from "../assets/images/quiz-preview-img.png";
export default {
      QUIZZES :[
        {   
            id: '123123123',
            createdAt:'Mar 1, 2021',
            createdBy: 'Haythem Ferjaoui',
            title: 'Do you have what it takes to make your blog blast-off?',
            questions: [
                {
                  name: "How does your best friend usually describe you?",
                  image: quizPrevieImg,
                  description:
                    "Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen!",
                },
                {
                    name: "quiz 1 question 2",
                  image: quizPrevieImg,
                  description: "Probably the most random thing you have ever seen!",
                },
                {
                    name: "quiz 1 question 3",
                  image: quizPrevieImg,
                  description: "Probably the most random thing you have ever seen!",
                }
      ]},
        {   
            id: '1231234223123',
            createdAt:'Mar 1, 2021',
            createdBy: 'Haythem Ferjaoui',
            title: 'Do you have what it takes to make your blog blast-off?',
            questions: [
                {
                    name: "quiz 2 question 1",
                  image: quizPrevieImg,
                  description:
                    "Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen!",
                },
                {
                    name: "quiz 2 question 2",
                  image: quizPrevieImg,
                  description: "Probably the most random thing you have ever seen!",
                },
                {
                    name: "quiz 2 question 3",
                  image: quizPrevieImg,
                  description: "Probably the most random thing you have ever seen!",
                }
      ]},
        {   
            id: '12312312663',
            createdAt:'Mar 1, 2021',
            createdBy: 'Haythem Ferjaoui',
            title: 'Do you have what it takes to make your blog blast-off?',
            questions: [
                {
                  name: "quiz 3 question 1",
                  image: quizPrevieImg,
                  description:
                    "Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen! Probably the most random thing you have ever seen!",
                },
                {
                    name: "quiz 3 question 2",
                  image: quizPrevieImg,
                  description: "Probably the most random thing you have ever seen!",
                },
                {
                    name: "quiz 3 question 3",
                  image: quizPrevieImg,
                  description: "Probably the most random thing you have ever seen!",
                }
      ]}
    ]
}