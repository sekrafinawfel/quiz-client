import "./index.scss";
import mChoiceIcon from '../../assets/icons/m-choice.svg';
import fBlankIcon from '../../assets/icons/f-blank.svg';
const SelectQuizType = () => {
  return (
    <div className="select-quiz-container">
        <div className="m-choice">
            <img src={mChoiceIcon} alt="multiple choice illustration" />
            <div className="text">
                <h4>Multiple choice</h4>
                <span>Check retention by asking students to choose one or more correct answers. Use text, images, or math equations to spice things up!</span>
            </div>
            <button className="select">
                Select
            </button>
        </div>
        <div className="f-blank">
        <img src={fBlankIcon} alt="fill in the blancs illustration" />
            <div className="text">
                <h4>Fill in the Blancs</h4>
                <span>prompt your students to enter text and check if they remember the correct spelling of the accommodation.</span>
            </div>
            <button className="select">
                Select
            </button>
        </div>
    </div>
  )
}

export default SelectQuizType