import React, { useRef, useState } from "react";
import "./index.scss";
import boltIcon from "../../assets/icons/bolt.svg";
import addNewQuizIcon from "../../assets/icons/newQuiz.svg";
import Divider from "@mui/material/Divider";
import searchIcon from "../../assets/icons/search.svg";
import Pagination from "@mui/material/Pagination";
import { NavLink } from "react-router-dom";
import Carousel from "react-material-ui-carousel";
import constants from "../../utilities/constants";

import SliderItem from "../../components/SliderItem";
import QuizListItem from "../../components/QuizListItem";

const quizzes = constants.QUIZZES;

const QuizList = () => {
  const [showPreview, setShowPreview] = useState(true);
  const [questionsToShow, setQuestionsToShow] = useState([]);
  const hideModal = () => {
    setShowPreview(!showPreview);
  };
  const showQuizPreview =(questions) =>{
    setQuestionsToShow(questions)
  }
  
 

  const inputElement = useRef();
  const focusInput = () => {
    inputElement.current.focus();
  };

  return (
    <div
      className="quizList-container"
    >
      <div className="quiz-list">
        <div className="card-header">
          <div>
            <img src={boltIcon} alt="quiz list icon" />
            <span>Quizzes</span>
          </div>
          <NavLink to="/select-quiz-type">
          <button >
            <img src={addNewQuizIcon} alt="new quiz icon" />
            <span>Create new quiz</span>
          </button>
          </NavLink>
        </div>
        <Divider />
        <div className="search-sort">
          <div className="sort-container">
            <span>Sort by:</span>
            
              <select className="select-date">
                <option value="createdAt">date</option>
              </select>
              
            <div className="select-filter">Filter</div>
          </div>
          <div className="search" onClick={focusInput}>
            <input
              type="text"
              ref={inputElement}
              placeholder="Search..."
              name=""
              id=""
            />
            <img src={searchIcon} alt="" />
          </div>
        </div>
        <div className="list">
          {
            quizzes.map((quiz) => {
              return <QuizListItem {...quiz} showQuizPreview={showQuizPreview} key={quiz.id}/>
            })
          }
          
        </div>
        <div className="pagination">
          <Pagination count={10} shape="rounded" />
        </div>
      </div>
      <div className="quiz-preview">
        <Carousel>
          {questionsToShow.map((item, i) => (
            <SliderItem
              key={i}
              item={item}
              length={questionsToShow.length}
              hideModal={hideModal}
              indexValue={i}
            />
          ))}
        </Carousel>
      </div>
    </div>
  );
};


export default QuizList;
