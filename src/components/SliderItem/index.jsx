import closeIcon from "../../assets/icons/close-icon.svg";
import trueChoice from "../../assets/icons/trueChoice.svg";

import "./index.scss"
const SliderItem = ({ indexValue, hideModal, item, length }) => {
  return (
          <div className="slider-item" key={indexValue}>
            <img
              src={closeIcon}
              className="closeModalBtn"
              onClick={() => hideModal()}
              alt=""
            />
            <h2>{item.name}</h2>
            <img src={item.image} alt="" />
            {/* <span className="description">{props.item.description}</span> */}
            <div className="choices">
              <div className="choice">
                <div className="checkBtn">
                  <img src={trueChoice} />
                </div>
                <span>How does your best friend usually describe you</span>
              </div>
              <div className="choice isTrue">
                <div className="checkBtn">
                  <img src={trueChoice} />
                </div>
                <span>How does your best friend usually describe you</span>
              </div>
              <div className="choice">
                <div className="checkBtn">
                  <img src={trueChoice} />
                </div>
                <span>How does your best friend usually describe you</span>
              </div>
            </div>
            <div className="page-counter">{indexValue + 1 + "/" + length}</div>
          </div>
        );
}

export default SliderItem