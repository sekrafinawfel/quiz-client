import moreIcon from "../../assets/icons/more.svg";
import closeIcon from "../../assets/icons/close-icon.svg";
import { NavLink } from "react-router-dom";
import "./index.scss";
const QuizListItem = ({createdAt, title, id, createdBy, questions, showQuizPreview}) => {
  return (
    <div className="quiz-list-item">
            <div className="details">
              <h2 className="title">
              {title}
              </h2>
              <div className="info">
                <span className="createdAt">Created {createdAt}</span>
                <span className="createdBy">By: {createdBy}</span>
                <div className="categories">
                  <div className="category orange">
                    <span>Math</span>
                    <img src={closeIcon} />
                  </div>
                  <div className="category green">
                    <span>English</span>
                    <img src={closeIcon} />
                  </div>
                </div>
              </div>
            </div>
            <div className="actions">
              <NavLink to={`${id}`} className="link-btn">
                <button className="btn blue-btn" >
                  Edit
                </button>
              </NavLink>

              <button className="btn blue-btn">Share</button>
              <button
                className="btn green-btn"
                onClick={()=> showQuizPreview(questions)}
              >
                Preview
              </button>
              <img src={moreIcon} className="more-btn" />
            </div>
          </div>
  )
}

export default QuizListItem